# YA.P
**Course python(ru)**

1. Intro Python

1.1. Git

    1.1.1 Git
        - ЛК. 1.1.1 Сисдемы контроля версий (Л 2)
        - ПЗ. 1.1.1 Использование Git (ПЗ 2)
        - ПЗ. 1.1.2 Продвинутое использование Git (ПЗ 2)
    
    1.1.2 Gitlab. IDE
        - ЛК. 1.1.2 Gitlab. VScode (Л 2)
        - ПЗ. 1.1.2 Тренажер по использованию Git (ПЗ 4)

1.2 Введение в ЯП

        - ЛК. 1.2 Введение в python (Л 2)
        - ПЗ. 1.2.1 Управляющие символы (ПЗ 2)
        - ПЗ. 1.2.2 Ввод, вывод. Преобразование типов (ПЗ 2)
        - ПЗ. 1.2.3 Арифметические опреации (ПЗ 2)
        - ПЗ. 1.2.4 Введение в типы данных (ПЗ 2)

1.3. Условия (добавить практические примеры)

        - ЛК. 1.3 Условия в языке программирования python (Л 2)
        - ПЗ. 1.3 Инструкции выбора и множественного выбора (ПЗ 4)
      
1.4. Списки строки

        - ЛК. 1.4.1 Строки (Л 2)
        - ЛК. 1.4.2 Списки (Л 2)
        - ПЗ. 1.4.1 Строки (ПЗ 2)
        - ПЗ. 1.4.2 Списки (ПЗ 2)
        - ПЗ. 1.4.2 Практика по использованию списков и строк (ПЗ 2)

1.5. Циклы 

        - ЛК. 1.5.1 Циклы (Л 2)
        - ЛК. 1.5.2 Вложенные циклы (Л 2)
        - ПЗ. 1.5.1 Циклы (ПЗ 6)
        - ПЗ. 1.5.2 Вложенные циклы. Введение в модули на примере random (ПЗ 6)
   

2. Easy Python

2.1. Кортежи. Множества. Словари

        - ЛК. 2.1.1. Множества. Кортежи (Л 2)
        - ЛК. 2.1.2. Словари (Л 2)
        - ЛК. 2.1.3. Введение в структуры данных (Л 2)
        - ПЗ. 2.1.1. Обобщение основ python (ПЗ 2) -
        - ПЗ. 2.1.2. Множества (ПЗ 2) 
        - ПЗ. 2.1.3. Кортежи (ПЗ 2) 
        - ПЗ. 2.1.4. Словари (ПЗ 2)

2.2. Функции
    - ЛК. 
    - ПЗ. 2.1.1.


8/16 ЛК 
17/34 ПЗ


# M.P 
1. Advanced PEP3
1.1. Продвинутое использование функций
    - ЛК. Lambda Декораторы Замыкания
    - ПЗ. 2.2.1. Lambda функции  
    - ПЗ. 2.2.2. Декораторы Замыканияи  

1.2 Продвинутые модули
intertools
os
и т.д.
hk


1.3 ООП SOLID PEP3

1.4 BIG O Алгоритмы 

Практические задания для изучения логических операторов python
Условные операторы с множественным вложением

# Создание виртуального окружения
apt install python3.13

apt install python3.13-venv

sudo ln -s /usr/bin/python3.13 /usr/bin/python

python -m venv venv

source venv/bin/activate

pip freeze

pip freeze > requirements.txt

deactivate

# Ресурсы для дополнительного изучения
1. https://habr.com/ru/articles/701098/

https://habr.com/ru/articles/725930/
http://ti.math.msu.su/wiki/doku.php?id=algo:algo

http://aisd.kubsau.ru/labs.html

http://repo.ssau.ru/bitstream/Metodicheskie-materialy/Laboratornyi-praktikum-po-programmirovaniu-na-yazyke-Python-Elektronnyi-resurs-metod-ukazaniya-71336/1/%d0%a0%d1%83%d0%b1%d1%86%d0%be%d0%b2%d0%b0%20%d0%a2.%d0%9f.%20%d0%9f%d1%80%d0%be%d0%b3%d1%80%d0%b0%d0%bc%d0%bc%d0%b8%d1%80%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5%20%d0%bd%d0%b0%20%d1%8f%d0%b7%d1%8b%d0%ba%d0%b5%20Python.pdf

https://pythonim.ru/

http://www.ontogovorun.ru

https://realpython.com/start-here/

https://python-course.readthedocs.io

Algoritm
https://github.com/TheAlgorithms/Python/blob/master/DIRECTORY.md


# Golang
**Course go(ru)**

### ОСНОВЫ

*Установка, типы данных и переменные*

_Установка и настройка_
- установка golang
- встроенные переменные в go
- каталоги bin src
- первая программа
- способы запуска кода
- сборка, пути до файла
- настройка vscode для работы с go

_Типы данных и переменные_

- типы
- переменные константы
- области видимости

_Ввод, вывод fmt_


