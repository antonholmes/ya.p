import requests
import time
import urllib3
from bs4 import BeautifulSoup
import base64
from PIL import Image
import io
import re
import os
import csv

from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy_utils import database_exists, create_database
import pandas as pd



url = 'https://nemez1da.ru/'
headers = requests.utils.default_headers()
headers.update(
    {
        'User-Agent': 'My User Agent 1.0',
    }
)
response = requests.get(url, headers=headers)
soup = BeautifulSoup(response.text, features="lxml")
rows = soup.find_all('a', {"class": "page-numbers"})

lst=[]
for t in rows:
    x = t.text.replace(u'\xa0', u'')
    try:
        lst.append(int(x))
    except ValueError:
        pass
max_count=max(lst)
print('Количество странци с персоналими ', max_count)

#Получаем все ссылки на персоналии
ls_href=[]
timeout = 180 
# Поставить max_count в ЦИКЛЕ
for i in range(1, max_count + 1): 
    url_search_page = f'https://nemez1da.ru/page/{i}/'
    print(url_search_page)
    
    with requests.Session() as session:
        response = session.get(url_search_page, headers=headers, timeout=timeout)
    
    if response.status_code == 200:
        soup = BeautifulSoup(response.content, 'html.parser')
        page_links = soup.find_all("h3", {"class": "simple-grid-grid-post-title"})
        
        for link in page_links:
            print(link.text)  # Вывод текста ссылки
            print(link.a['href'])  # Вывод URL ссылки
        
    else:
        print(f"Ошибка при получении страницы {url_search_page}")

    for page_link in page_links:
        a_tag = page_link.find('a') 
        if a_tag:
        # Получаем значение атрибута 'href' у тега <a>
            href = a_tag.get('href')
            ls_href.append(href)
print(len(ls_href))

with open('output.txt', 'w') as file:
    # Записываем каждую ссылку в файл
    for href in ls_href:
        file.write(href + '\n')
# можно дописать условие добавления ссылок на персонажей которых нет в txt

pattern_fuilds = r'<b>(.*?)</b>'
data_fuild_users = set()

max_retries = 4
retry_delay = 5  # Задержка между попытками подключения (в секундах)

for retry in range(max_retries):
    try:
        for z in range(1000):
            # поменять на range(1000) range(len(ls_href)):
            url_search_page = ls_href[z]
            response = requests.get(url_search_page, headers=headers, timeout=timeout)
    # soup = BeautifulSoup(response.content, 'html.parser')
            soup = BeautifulSoup(response.text, features="lxml")
            data_html_fuild = soup.find('div', {"class": "entry-content simple-grid-clearfix"})
            try:
                photo_link = data_html_fuild.find_all('b')
            except AttributeError as e:
    # Обработка ошибки AttributeError
                print("Произошла ошибка AttributeError:", e)
    # Дополнительные действия, которые могут быть выполнены при возникновении ошибки
                pass  # Пропустить выполнение кода или выполнить другие действия
            for data in photo_link:
                data_fuild_users.add(data.text)
            print(ls_href[z])
            print(data_fuild_users)
        if response.status_code == 200:
            print("Успешное подключение!")
            break  # Выход из цикла после успешного подключения
    except requests.exceptions.RequestException as e:
        print(f"Ошибка подключения: {e}")
    
    if retry < max_retries - 1:
        print(f"Повторная попытка подключения через {retry_delay} секунд...")
        time.sleep(retry_delay)
    else:
        print("Превышено количество попыток подключения")


data_fuild_users.update(['FIO', 'base64'])
# Может не понадобиться
# with open('folder.txt', 'w') as file:
#     file.write(str(data_fuild_users))

    # Заголовки
sorted_headers = sorted(list(data_fuild_users))

with open('data.csv', 'a+', newline='') as file:
    writer=csv.DictWriter(file, fieldnames=sorted_headers)
    # for row in headers_List:
    # Попробовать закомментировать
    writer.writeheader()


for k in range(len(ls_href)):
    print('+++++++++++++++++++++++++++++++++++')
    url_search_page = ls_href[k]
    response = requests.get(url_search_page, headers=headers, timeout=timeout)
    soup = BeautifulSoup(response.text, features="lxml")

    fio = soup.find('h1')
    if fio is not None:
        a_tag = fio.find('a')
        if a_tag is not None:
            fio = a_tag.text
        else:
            fio = 'null'
    else:
        fio = 'null'

    try:
        photo = soup.find('div', {"class": "photos_single_place"})
        if photo is not None:
            print("Элемент найден успешно.")
            photo_link = photo.find_all('a')
            for link in photo_link:
                num = link['href']
            image_url = num    
            response = requests.get(image_url, headers=headers, timeout=timeout)

            if response.content:
                try:
                    image = Image.open(io.BytesIO(response.content))
        # Далее можно продолжать работу с изображением
                except Exception as e:
                    print(f"Error opening image: {e}")
            else:
                print("Image content is empty or None")

            image = Image.open(io.BytesIO(response.content))

            img_byte_arr = io.BytesIO()
            image.save(img_byte_arr, format='JPEG')
            img_byte_arr = img_byte_arr.getvalue()
            base64_image = base64.b64encode(img_byte_arr).decode('utf-8')
        else:
            base64_image = 'null'
    except AttributeError as e:
        print("Ошибка: Элемент не найден. Проверьте правильность запроса.")


    fields = soup.find_all('div', {"class": "entry-content simple-grid-clearfix"})
# # print(fields)
    category1=[]
    category1_data=[]
    for li in fields:
#     # Обращаемся к дочерним элементам <a> напрямую
        direct_a_children = li.find_all('div', recursive=False)

        for div_tag in direct_a_children:
            div_text = div_tag
            category1.append(str(div_text))
         # print(str(div_text))
        count_category = len(category1)
        category1_data = category1[1:count_category]

    # print(category1_data)      

    pattern = r'<b>(.*?)</b>'
    pattern2 = r'</b>\s*(.*?)</div>'

#     fieldnames = ['Link_face', 'FIO', 'Category_title', 'Image_link', 'base64']
#     info = {'Link_face': ls_href[k], 'FIO': fio.a.text, 'Category_title': category.a.text, 'Image_link':image_url, 'base64': base64_image}
    dict1 = {}
    for i in category1_data:
        match = re.findall(pattern, i)

        
        text_after_second_b = re.sub(r'<br/>', ', ', i)
        match2 = re.findall(pattern2, text_after_second_b)
        for word in match2:
            match3 = re.sub(r'<a href="(.*?)">', '', word)
            match4 = re.sub(r'</a>', '', match3)
            match5 = re.sub(r';', ',', match4)

        # print(match, match4)
        dict1.update({''.join(match):match5})

    addon={'FIO': fio, 'base64': base64_image }
    #     # base64_image
    dict1.update(addon)

    # Словарь с данными
    sorted_dict = dict(sorted(dict1.items(), key=lambda x: x[0]))
    print(sorted_dict)

    # Заголовки
    sorted_headers = sorted(list(data_fuild_users))

    with open('data.csv', 'a+', newline='') as file:
        writer=csv.DictWriter(file, fieldnames=sorted_headers)
    # for row in headers_List:
        writer.writerow({key: sorted_dict.get(key, 'null') for key in sorted_headers})


engine = create_engine("postgresql+psycopg2://admin:admin@localhost:15433/peoples")
if not database_exists(engine.url):
    create_database(engine.url)

engine.connect()
print(database_exists(engine.url))

print(engine)

Session = sessionmaker(bind=engine)
session = Session()

df = pd.read_csv('data_1.csv')
df.to_sql('test3', engine, if_exists='replace', index=False)
session.commit()

# Закрываем сессию
session.close()