> Пошаговое изучение golang
    
    1) https://stepik.org/course/100208
    2) https://go.dev/tour/list + english (soft skill)
    3) https://stepik.org/course/54403/info
    4) https://stepik.org/course/187490
    5) https://code-basics.com/ru/languages/go --- Для закрепления

> Дополнительные темы расширяющие первый курс (Подтянуть материал) 

    1) форматные спецификаторы
    2) указатели и структуры (методы)
    3) awesome go

> Статический материал про golang на русском

    https://metanit.com/go/
    https://golangify.com/go
    https://golang-blog.blogspot.com/

> Дополнительные источники 
    
    https://golangify.com/
    https://proglib.io/u/gerakolen/posts

> Источники для продвинутого изучения

    https://proglib.io/p/43-resursa-dlya-izucheniya-golang-v-2021-godu-sayty-blogi-soobshchestva-kursy-kanaly-i-knigi-2021-07-20
