### Установка golang Linux

Скачиваем (если ссылка нерабочая зайдите на официальный сайт установки)
wget https://go.dev/dl/go1.19.3.linux-amd64.tar.gz

Удаляем если был установлен golang и распаковываем архив
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go1.19.3.linux-amd64.tar.gz

Устанавливаем переменную окружения 
export PATH=$PATH:/usr/local/go/bin

Проверяем установку

└─$  go version
go version go1.19.3 linux/amd64

Создаем файл hello.go

```
package main

import "fmt"

func main() {
    fmt.Println("hello world")
}

```

Выполняем (вы должны находиться в каталоге исполняемого файла, либо прописывайте абсолютный путь)
go run parce.go


### Установка IDE Goland

***********************************************
Качаем 
https://www.jetbrains.com/go/download/#section=linux

Если ссылка нерабочая используйте vpn, либо спросите

***********************************************
Извлекаем 
Extract the tarball to a directory that supports file execution.

For example, if the downloaded version is 1.17.7391, you can extract it to the recommended /opt directory using the following command:
sudo tar -xzf jetbrains-toolbox-1.17.7391.tar.gz -C /opt

> Название файла соответствует вашему

********************************************
Качаем ключ
https://blog.llinh9ra.ru/%d1%81%d0%be%d1%84%d1%82/%d0%b0%d0%ba%d1%82%d0%b8%d0%b2%d0%b0%d1%86%d0%b8%d1%8f-phpstorm-webstorm-intellij-idea-%d0%b8-%d0%b4%d1%80%d1%83%d0%b3%d0%b8%d0%b5-%d0%bf%d1%80%d0%be%d0%b4%d1%83%d0%ba%d1%82%d1%8b-jetbrains-%d0%b2/#comments

Излекаем
unzip jetbra-ded4f9dc4fcb60294b21669dafa90330f2713ce4.zip 

Устанавливаем
./scripts/install.sh 


Установка golang WindowsУстановка golang Linux
