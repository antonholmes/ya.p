# Bootstrap

### Инструкция по установке bootstrap


1. **Загрузка Bootstrap**:
   - Перейдите на официальный сайт Bootstrap: https://getbootstrap.com/.
   - Нажмите на кнопку "Download" для загрузки комплекта Bootstrap.
   - Выберите, хотите ли загрузить скомпилированный CSS и JavaScript, или же исходные файлы для настройки и компиляции.

2. **Подключение Bootstrap к вашему проекту**:
   - Разархивируйте скачанный файл Bootstrap, если это необходимо.
   - Скопируйте содержимое папки "css" (файл bootstrap.min.css) и "js" (файлы bootstrap.min.js и jquery.min.js) в ваш проект.
   - Для подключения Bootstrap к вашему HTML-файлу, добавьте следующие строки в секцию `` вашего документа:
     

```html
     <link rel="stylesheet" href="путь_к_файлу/bootstrap.min.css">
     <script src="путь_к_файлу/jquery.min.js"></script>
     <script src="путь_к_файлу/bootstrap.min.js"></script>
```     

     Замените "путь_к_файлу" на путь к вашим файлам Bootstrap в вашем проекте.

3. **Проверка**:
   - Откройте ваш HTML-файл в браузере и убедитесь, что Bootstrap успешно подключен.
   - Для проверки, попробуйте добавить какой-либо компонент Bootstrap на вашу страницу и убедитесь, что он отображается корректно.

**Урок 1: Основы Bootstrap**

1. **Подключение Bootstrap**:
   - Следуйте инструкции по установке Bootstrap, описанной выше, чтобы подключить комплект Bootstrap к вашему проекту.

2. **Использование сетки (Grid System)**:
   - Bootstrap использует сетку с 12 колонками для удобного размещения контента на странице.
   - Создайте контейнер с классом `.container` или `.container-fluid`.
   - Используйте классы `.row` и `.col-*` для размещения элементов в сетке, где `*` - это количество колонок, которые элемент должен занимать.
   - Пример:
     

```html
     <div class="container">
         <div class="row">
             <div class="col-md-6">Поле 1</div>
             <div class="col-md-6">Поле 2</div>
         </div>
     </div>
```     

3. **Использование компонентов**:
   - Bootstrap предоставляет множество готовых компонентов, таких как кнопки, формы, навигационные панели и т. д.
   - Используйте классы Bootstrap для стилизации и размещения компонентов на вашей странице.
   - Пример кнопки:
     

html
     <button class="btn btn-primary">Нажми меня</button>
     

4. **Использование навигационной панели (Navbar)**:
   - Создайте навигационную панель с помощью класса `.navbar`.
   - Добавьте элементы навигации внутри блока `.navbar`.
   - Пример:
     

html
     <nav class="navbar navbar-expand-lg navbar-light bg-light">
         <a class="navbar-brand" href="#">Мой сайт</a>
         <ul class="navbar-nav">
             <li class="nav-item"><a class="nav-link" href="#">Главная</a></li>
             <li class="nav-item"><a class="nav-link" href="#">О нас</a></li>
         </ul>
     </nav>
     

5. **Использование адаптивности**:
   - Bootstrap обеспечивает адаптивный дизайн, который автоматически подстраивается под различные устройства.
   - Используйте классы `.col-*`, `.container`, `.container-fluid` для создания адаптивного макета.
   - Для адаптивных классов используйте префиксы `sm`, `md`, `lg`, `xl` для определения поведения на различных разрешениях экрана.

**Урок 1: Продвинутое использование Bootstrap**

1. **Использование JavaScript компонентов**:
   - Bootstrap предоставляет множество JavaScript компонентов, таких как модальные окна, вкладки, аккордеоны и др.
   - Для использования JavaScript компонентов, подключите файл `bootstrap.min.js` к вашему проекту.
   - Пример модального окна:
     

```html
     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
         Открыть модальное окно
     </button>

     <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-header">
                     <h5 class="modal-title" id="exampleModalLabel">Модальное окно</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">×</span>
                     </button>
                 </div>
                 <div class="modal-body">
                     Содержимое модального окна здесь.
                 </div>
                 <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                     <button type="button" class="btn btn-primary">Сохранить изменения</button>
                 </div>
             </div>
         </div>
     </div>
```    

2. **Использование настраиваемых переменных**:
   - Bootstrap 4 включает возможность настройки переменных SCSS для изменения цветов, шрифтов, отступов и других стилей.
   - Создайте свой собственный файл `custom.scss` и определите настраиваемые переменные до импорта Bootstrap.
   - Пример настройки цветов:
     

```scss
     $primary: #ff0000;
     $secondary: #00ff00;

     @import 'bootstrap/bootstrap';
```    

3. **Использование сетки с различными устройствами**:
   - Bootstrap позволяет настраивать сетку для различных устройств, определяя количество колонок для каждого разрешения.
   - Используйте классы `.col-*` с префиксами `sm`, `md`, `lg`, `xl` для создания адаптивного макета.
   - Пример:
     

```html
     <div class="container">
         <div class="row">
             <div class="col-md-6 col-lg-4">Поле 1</div>
             <div class="col-md-6 col-lg-4">Поле 2</div>
             <div class="col-lg-4 d-none d-lg-block">Поле 3 (только для lg и больше)</div>
         </div>
     </div>
```     

4. **Использование дополнительных расширений**:
   - Используйте дополнительные расширения и инструменты, такие как Bootstrap Icons, Bootstrap Themes, и т. д., для расширения функциональности Bootstrap.
   - Интегрируйте различные инструменты и библиотеки с Bootstrap для создания более сложных и интерактивных веб-приложений.

Используйте документацию Bootstrap (https://getbootstrap.com/docs/4.6/getting-started/introduction/) для изучения дополнительных компонентов и возможностей. 