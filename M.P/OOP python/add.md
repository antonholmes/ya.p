Оригинальный класс называется *родительским классом*, *суперклассом* или *базовым классом*, а новый класс именуется *потомком*, *подклассом* или *производным классом**.

Определим пустой класс, который называется Car. Далее мы определим подкласс класса Car, который называется Yugo

>>> class Car():
... pass
...
>>> class Yugo(Car):
... pass


Проверить, унаследован ли класс от другого класса, можно с помощью функции  issubclass():

>>> issubclass(Yugo, Car)
True



В.нашем.случае.классы.Car.и.Yugo.абсолютно бесполезны,.поэтому.попробуем.новые.определения

class Car():
    def exclaim(self):
    print("I'm a Car!")

class Yugo(Car):
    pass

>> give_me_a_car = Car()
>>> give_me_a_yugo = Yugo()
>>> give_me_a_car.exclaim()
I'm a Car!
>>> give_me_a_yugo.exclaim()
I'm a Car!


класс.Yugo.унаследовал.метод.exclaim()

### Переопределение методов

Изменим.способ.работы.метода.exclaim().
для.класса.Yugo


>>> class Car():
... def exclaim(self):
... print("I'm a Car!")
...
>>> class Yugo(Car):
... def exclaim(self):
... print("I'm a Yugo! Much like a Car, but more Yugo-ish."

# после создания объекта класса видим переопределение метода
>> give_me_a_car = Car()
>>> give_me_a_yugo = Yugo()
>>> give_me_a_car.exclaim()
I'm a Car!
>>> give_me_a_yugo.exclaim()
I'm a Yugo! Much like a Car, but more Yugo-ish.


Переопределять.можно.лю-
бые.методы,.включая.__init__().


>>> class Person():
... def __init__(self, name):
... self.name = name
...
>>> class MDPerson(Person):
... def __init__(self, name):
... self.name = "Doctor " + name
...
>>> class JDPerson(Person):
... def __init__(self, name):
... self.name = name + ", Esquire


В.этих.случаях.метод.инициализации.__init__().принимает.те.же.аргументы,.
что.и.родительский.класс.Person,.но.по-разному.сохраняет.значение.переменной.
name.внутри.объекта:

>>> person = Person('Fudd')
>>> doctor = MDPerson('Fudd')
>>> lawyer = JDPerson('Fudd')
>>> print(person.name)
Fudd
>>> print(doctor.name)
Doctor Fudd
>>> print(lawyer.name)
Fudd, Esquire

### Добавление метода 

В.производный.класс.можно.добавить.и.метод,.которого.не.было.в.родительском.
классе.

>>> class Car():
... def exclaim(self):
... print("I'm a Car!")

 class Yugo(Car):
... def exclaim(self):
... print("I'm a Yugo! Much like a Car, but more Yugo-ish.")
... def need_a_push(self):
... print("A little help here?")


Далее.создадим.объекты.классов.Car.и.Yugo:
>>> give_me_a_car = Car()
>>> give_me_a_yugo = Yugo()
Объект.класса.Yugo.может.реагировать.на.вызов.метода.need_a_push():
>>> give_me_a_yugo.need_a_push()
A little help here?
А.объект.общего.класса.Car.—.нет:
>>> give_me_a_car.need_a_push()
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
AttributeError: 'Car' object has no attribute 'need_a_push'
