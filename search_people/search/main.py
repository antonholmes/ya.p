import requests
import urllib3
from bs4 import BeautifulSoup
import base64
from PIL import Image
import io
import re
import os
import pymongo


url = 'https://nemez1da.ru/'
headers = requests.utils.default_headers()
headers.update(
    {
        'User-Agent': 'My User Agent 1.0',
    }
)
response = requests.get(url, headers=headers)
soup = BeautifulSoup(response.text, features="lxml")
rows = soup.find_all('a', {"class": "page-numbers"})

lst=[]
for t in rows:
    x = t.text.replace(u'\xa0', u'')
    try:
        lst.append(int(x))
    except ValueError:
        pass
max_count=max(lst)
print('Количество странци с персоналими ', max_count)

#Получаем все ссылки на персоналии
ls_href=[]
timeout = 30 
# Поставить max_count в ЦИКЛЕ
for i in range(1, max_count + 1): 
    url_search_page = f'https://nemez1da.ru/page/{i}/'
    print(url_search_page)
    
    with requests.Session() as session:
        response = session.get(url_search_page, headers=headers, timeout=timeout)
    
    if response.status_code == 200:
        soup = BeautifulSoup(response.content, 'html.parser')
        page_links = soup.find_all("h3", {"class": "simple-grid-grid-post-title"})
        
        for link in page_links:
            print(link.text)  # Вывод текста ссылки
            print(link.a['href'])  # Вывод URL ссылки
        
    else:
        print(f"Ошибка при получении страницы {url_search_page}")

    for page_link in page_links:
        a_tag = page_link.find('a') 
        if a_tag:
        # Получаем значение атрибута 'href' у тега <a>
            href = a_tag.get('href')
            ls_href.append(href)
            # print(ls_href)
print(len(ls_href))

with open('output.txt', 'w') as file:
    # Записываем каждую ссылку в файл
    for href in ls_href:
        file.write(href + '\n')

# Перебор всех  

# print('Ссылка на персонажа ' + ls_href[10])
client = pymongo.MongoClient("mongodb://mongo:27017/")
db = client["mydatabase"]
collection = db["mycollection"]

for k in range(len(ls_href)):
    url_search_page = ls_href[k]
    response = requests.get(url_search_page, headers=headers)
    soup = BeautifulSoup(response.text, features="lxml")
    fio = soup.find('h1')
#Получили ФИО Записываем каждый элемент списка в файл
    # print('ФИО ' + fio.a.text)
    
    category = soup.find('span', {"class": "simple-grid-entry-meta-single-cats"})
    # print('Категория ' + category.a.text)
#Получили категорию
    photo = soup.find('div', {"class": "photos_single_place"})
    photo_link = photo.find_all('a')
    for link in photo_link:
        num = link['href']
        # print(num)

#Получили ссылку на фото
    image_url = num    
    response = requests.get(image_url, headers=headers)
    image = Image.open(io.BytesIO(response.content))

# Преобразование в байты
    img_byte_arr = io.BytesIO()
    image.save(img_byte_arr, format='JPEG')
    img_byte_arr = img_byte_arr.getvalue()

# Кодирование изображения в base64
    base64_image = base64.b64encode(img_byte_arr).decode('utf-8')
    print('base64  ' + base64_image)

    fields = soup.find_all('div', {"class": "entry-content simple-grid-clearfix"})
# print(fields)
    category1=[]
    category1_data=[]
    for li in fields:

        direct_a_children = li.find_all('div', recursive=False)

        for div_tag in direct_a_children:
            div_text = div_tag
            category1.append(str(div_text))
        # print(str(div_text))
        count_category = len(category1)
        category1_data = category1[1:count_category]        

    pattern = r'<b>(.*?)</b>'
    pattern2 = r'</b>\s*(.*?)</div>'

    fieldnames = ['Link_face', 'FIO', 'Category_title', 'Image_link', 'base64']
    info = {'Link_face': ls_href[k], 'FIO': fio.a.text, 'Category_title': category.a.text, 'Image_link':image_url, 'base64': base64_image}
    for i in category1_data:
        match = re.findall(pattern, i)
        text_after_second_b = re.sub(r'<br/>', ', ', i)
        match2 = re.findall(pattern2, text_after_second_b)
        for word in match2:
            match3 = re.sub(r'<a href="(.*?)">', '', word)
            match4 = re.sub(r'</a>', '', match3)
        # print(match, match4)
        test = ''
        
        # for key in info.keys():
        #     if key not in fieldnames:
        fieldnames.append(test.join(match))
        ###

        dct2= {test.join(match):match4}
        info.update(dct2)
    insert_result = collection.insert_one(info)
    print('+++++++')
    print(fieldnames)
    print('+++++++')



# Добавить все в csv файл 
# Обработать csv файл
# Создание БД 
# Пройти по спику и записать в БД