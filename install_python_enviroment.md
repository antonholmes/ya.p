# Установка python на Linux

### Проверяем текущую версию python
```bash
python3 --version || python --version
```
### Установка репозитория
```bash
sudo add-apt-repository -y ppa:deadsnakes/ppa
```
### Обновляем список возможных к установке пакетов и проверяем наличие python3.13
```bash
sudo apt-get update && apt-cache search python3.13
```

### Ищем где находится python
```bash
which python3
# /usr/bin/python3
```

### Выводим все пакеты и видим на какую текущую версию ссылается символьная ссылка ссылаеся текущая верс 
```bash
ls -l /usr/bin/python*
```
### Установка python3.13
```bash
sudo apt install python3.13
```

### Устанавливаем символьную ссылку
```bash
sudo ln -s /usr/bin/python3.13 /usr/bin/python
```

> Мы установили сисвольную ссылку по новому пути, который могут не определить различные IDE, если необходимо заменить именно python3, то удалите ссылку и установите заного

### Устанавливаем виртуальное окружение для версии 3.13
```bash
sudo apt install python3.13-venv
```

### Создание виртуального окружения
```bash
python -m venv venv
```

### Активирование виртуального окружения
```bash
source venv/bin/activate
```

### Деактивирование
```bash
deactivate
```

# Работа с менеждером пакетов 

pip pipenv pypi conda poetry